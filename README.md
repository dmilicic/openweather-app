# Overview

This app was built using an MVP structure. There is only 1 Activity and all other screens are fragments.
The app uses SQLite storage for storing bookmarks and a REST API for accessing Weather data.

The help screen explains on how to use the app.