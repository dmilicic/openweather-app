package com.dmilicic.weather.app.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * The weather model from OpenWeatherApp
 */
public class Weather implements Parcelable {

    private String mName;
    private double mLatitude;
    private double mLongitude;
    private String mMainForecast;
    private String mDescription;
    private double mTemperature;
    private double mTemperatureMin;
    private double mTemperatureMax;
    private double mPressure;
    private int mHumidity;
    private double mWindSpeed;
    private double mWindDeg;

    public Weather(String name, double latitude, double longitude, String mainForecast, String description,
                   double temperature, double temperatureMin, double temperatureMax, double pressure, int humidity,
                   double windSpeed, double windDeg) {
        mName = name;
        mLatitude = latitude;
        mLongitude = longitude;
        mMainForecast = mainForecast;
        mDescription = description;
        mTemperature = temperature;
        mTemperatureMin = temperatureMin;
        mTemperatureMax = temperatureMax;
        mPressure = pressure;
        mHumidity = humidity;
        mWindSpeed = windSpeed;
        mWindDeg = windDeg;
    }

    public String getName() {
        return mName;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getMainForecast() {
        return mMainForecast;
    }

    public String getDescription() {
        return mDescription;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public double getTemperatureMin() {
        return mTemperatureMin;
    }

    public double getTemperatureMax() {
        return mTemperatureMax;
    }

    public double getPressure() {
        return mPressure;
    }

    public int getHumidity() {
        return mHumidity;
    }

    public double getWindSpeed() {
        return mWindSpeed;
    }

    public double getWindDeg() {
        return mWindDeg;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "mName='" + mName + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mMainForecast='" + mMainForecast + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mTemperature=" + mTemperature +
                ", mTemperatureMin=" + mTemperatureMin +
                ", mTemperatureMax=" + mTemperatureMax +
                ", mPressure=" + mPressure +
                ", mHumidity=" + mHumidity +
                ", mWindSpeed=" + mWindSpeed +
                ", mWindDeg=" + mWindDeg +
                '}';
    }

    protected Weather(Parcel in) {
        mName = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mMainForecast = in.readString();
        mDescription = in.readString();
        mTemperature = in.readDouble();
        mTemperatureMin = in.readDouble();
        mTemperatureMax = in.readDouble();
        mPressure = in.readDouble();
        mHumidity = in.readInt();
        mWindSpeed = in.readDouble();
        mWindDeg = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
        dest.writeString(mMainForecast);
        dest.writeString(mDescription);
        dest.writeDouble(mTemperature);
        dest.writeDouble(mTemperatureMin);
        dest.writeDouble(mTemperatureMax);
        dest.writeDouble(mPressure);
        dest.writeInt(mHumidity);
        dest.writeDouble(mWindSpeed);
        dest.writeDouble(mWindDeg);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Weather> CREATOR = new Parcelable.Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };
}