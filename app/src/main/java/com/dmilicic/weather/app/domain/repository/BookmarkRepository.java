package com.dmilicic.weather.app.domain.repository;

import com.dmilicic.weather.app.presentation.model.Bookmark;

import java.util.List;

/**
 * Created by dario on 16.09.2017..
 */

public interface BookmarkRepository {

    interface Callback {
        void onBookmarksRetrieved(List<Bookmark> bookmarks);
    }

    void getAllBookmarks(BookmarkRepository.Callback callback);

    boolean saveBookmark(Bookmark bookmark);

    boolean deleteBookmark(Bookmark bookmark);

    boolean deleteBookmarkByName(String bookmarkName);

    boolean updateBookmark(Bookmark updated);

}
