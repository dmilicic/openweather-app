package com.dmilicic.weather.app.domain.repository;

import com.dmilicic.weather.app.domain.model.Weather;

/**
 * An interface repository with a getter for retrieving Weather data.
 */
public interface WeatherRepository {

    interface Callback {
        void onWeatherRetrieved(Weather weather);

        void onError();
    }

    void saveWeatherData(String name, Weather weather);

    void getWeatherData(double lat, double lon, Callback callback);
}
