package com.dmilicic.weather.app.network;

import com.dmilicic.weather.app.domain.repository.WeatherRepository;

/**
 * Created by dario on 14.09.2017..
 */

public abstract class RestClient {

    public static final String API_URL = "http://api.openweathermap.org/";
//    public static final String API_KEY = "75ff90f4d4b76e3038b45f7fbec6804a";
    public static final String API_KEY = "c6e381d8c7ff98f0fee43775817cf6ad"; // from Backbase



    public abstract void executeAsync(WeatherRepository repository, WeatherRepository.Callback callback);
}
