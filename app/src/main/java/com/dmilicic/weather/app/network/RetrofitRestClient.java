package com.dmilicic.weather.app.network;

import com.dmilicic.weather.app.domain.repository.WeatherRepository;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by dario on 14.09.2017..
 */

public class RetrofitRestClient extends RestClient {

    private Retrofit mRetrofit;

    private static RetrofitRestClient sRetrofitRestClient;

    private RetrofitRestClient(Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    public static RestClient getInstance() {
        if (sRetrofitRestClient == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestClient.API_URL)
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();

            sRetrofitRestClient = new RetrofitRestClient(retrofit);
        }

        return sRetrofitRestClient;
    }

    @Override
    public void executeAsync(WeatherRepository repository, WeatherRepository.Callback callback) {
        if (!(repository instanceof WeatherRepositoryImpl))
            throw new IllegalArgumentException("This repository must work with retrofit");

        WeatherRepositoryImpl weatherRepository = (WeatherRepositoryImpl) repository;
        weatherRepository.getAsync(getRetrofit(), callback);
    }

    public Retrofit getRetrofit() {
        if (mRetrofit != null){
            return mRetrofit;
        } else throw new IllegalStateException("Retrofit must be initialized!");
    }
}
