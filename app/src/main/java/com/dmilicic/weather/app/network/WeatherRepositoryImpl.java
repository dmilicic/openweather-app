package com.dmilicic.weather.app.network;

import com.dmilicic.weather.app.domain.model.Weather;
import com.dmilicic.weather.app.domain.repository.WeatherRepository;
import com.dmilicic.weather.app.network.converter.WeatherJsonConverter;
import com.dmilicic.weather.app.network.services.WeatherService;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by dario on 14.09.2017.
 */

public class WeatherRepositoryImpl implements WeatherRepository {

    private static final String METRIC = "metric";
    private double mLatitude, mLongitude;

    //region singleton
    private static WeatherRepository sWeatherRepository;

    private WeatherRepositoryImpl() {
    }

    public static WeatherRepository getInstance() {
        if (sWeatherRepository == null) {
            sWeatherRepository = new WeatherRepositoryImpl();
        }

        return sWeatherRepository;
    }
    //endregion


    @Override
    public void saveWeatherData(String name, Weather weather) {

    }

    @Override
    public void getWeatherData(double lat, double lon, WeatherRepository.Callback callback) {
        RetrofitRestClient restClient = (RetrofitRestClient) RetrofitRestClient.getInstance();

        mLatitude = lat;
        mLongitude = lon;

        restClient.executeAsync(this, callback);
    }

    public void getAsync(Retrofit retrofit, final Callback callback) {
        WeatherService service = retrofit.create(WeatherService.class);
        Call<String> call = service.getWeatherForLocationAsync(mLatitude, mLongitude, RestClient.API_KEY, METRIC);
        call.enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                String jsonResponse = response.body();

                // convert and notify our listener that we have the object
                callback.onWeatherRetrieved(WeatherJsonConverter.fromJson(jsonResponse));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}
