package com.dmilicic.weather.app.network.converter;

import com.dmilicic.weather.app.domain.model.Weather;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;


/**
 * Created by dario on 14.09.2017..
 *
 * This class is responsible for converting the OpenWeather api response to our local model.
 * The reason this approach was taken is because using Gson would require us to create a bunch of different classes for every nested value in the JSON.
 */

public class WeatherJsonConverter {

    //region example
    // Example JSON
//    {
//        "coord":{
//        "lon":0,
//                "lat":0
//    },
//        "weather":[
//        {
//            "id":800,
//                "main":"Clear",
//                "description":"clear sky",
//                "icon":"01d"
//        }
//        ],
//        "base":"stations",
//        "main":{
//                 "temp":24.17,
//                "pressure":1028.42,
//                "humidity":100,
//                "temp_min":24.17,
//                "temp_max":24.17,
//                "sea_level":1028.56,
//                "grnd_level":1028.42
//    },
//        "wind":{
//        "speed":2.41,
//                "deg":237.501
//    },
//        "clouds":{
//        "all":0
//    },
//        "dt":1505392890,
//            "sys":{
//        "message":0.0039,
//                "sunrise":1505368324,
//                "sunset":1505411914
//    },
//        "id":6295630,
//            "name":"Earth",
//            "cod":200
//    }
    //endregion

    public static Weather fromJson(String json) {

        JsonParser jsonParser = new JsonParser();
        JsonObject root = jsonParser.parse(json).getAsJsonObject();

        String name = "Unknown";
        JsonPrimitive jName = root.getAsJsonPrimitive("name");
        if (jName != null) name = jName.getAsString();

        JsonObject coord = root.getAsJsonObject("coord");
        double lon = coord.getAsJsonPrimitive("lon").getAsDouble();
        double lat = coord.getAsJsonPrimitive("lat").getAsDouble();

        JsonObject jsonWeather = root.getAsJsonArray("weather").get(0).getAsJsonObject();
        String mainForecast = jsonWeather.getAsJsonPrimitive("main").getAsString();
        String description = jsonWeather.getAsJsonPrimitive("description").getAsString();

        JsonObject main = root.getAsJsonObject("main");
        double temp = main.getAsJsonPrimitive("temp").getAsDouble();
        double pressure = main.getAsJsonPrimitive("pressure").getAsDouble();
        int humidity = main.getAsJsonPrimitive("humidity").getAsInt();
        double tempMin = main.getAsJsonPrimitive("temp_min").getAsDouble();
        double tempMax = main.getAsJsonPrimitive("temp_max").getAsDouble();;


        JsonObject wind = root.getAsJsonObject("wind");
        double speed = wind.getAsJsonPrimitive("speed").getAsDouble();

        JsonPrimitive jDeg = wind.getAsJsonPrimitive("deg");
        double deg = -1;
        if (jDeg != null && !jDeg.isJsonNull()) {
            deg = jDeg.getAsDouble();
        }

        return new Weather(name, lat, lon, mainForecast, description, temp, tempMin, tempMax, pressure, humidity, speed, deg); // might be null
    }
}
