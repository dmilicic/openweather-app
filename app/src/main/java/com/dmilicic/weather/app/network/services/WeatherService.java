package com.dmilicic.weather.app.network.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by dario on 14.09.2017..
 */

public interface WeatherService {

    @GET("/data/2.5/weather")
    Call<String> getWeatherForLocationAsync(@Query("lat") double lat, @Query("lon")double lon, @Query("appid") String apiKey, @Query("units") String units);
}
