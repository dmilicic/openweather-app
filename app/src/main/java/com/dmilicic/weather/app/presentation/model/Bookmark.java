package com.dmilicic.weather.app.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dario on 15.09.2017..
 */

public class Bookmark implements Parcelable {

    private String mBookmarkName;
    private String mLocationName;
    private double mLatitude;
    private double mLongitude;

    public Bookmark(String bookmarkName, double latitude, double longitude) {
        mBookmarkName = bookmarkName;
        mLatitude = latitude;
        mLongitude = longitude;

        mLocationName = ""; // let's avoid this being null
    }

    public Bookmark(String bookmarkName, String locationName, double latitude, double longitude) {
        mBookmarkName = bookmarkName;
        mLocationName = locationName;
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public String getBookmarkName() {
        return mBookmarkName;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public void setLocationName(String locationName) {
        mLocationName = locationName;
    }

    protected Bookmark(Parcel in) {
        mBookmarkName = in.readString();
        mLocationName = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mBookmarkName);
        dest.writeString(mLocationName);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Bookmark> CREATOR = new Parcelable.Creator<Bookmark>() {
        @Override
        public Bookmark createFromParcel(Parcel in) {
            return new Bookmark(in);
        }

        @Override
        public Bookmark[] newArray(int size) {
            return new Bookmark[size];
        }
    };
}