package com.dmilicic.weather.app.presentation.presenters;

import com.dmilicic.weather.app.domain.model.Weather;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.presentation.presenters.base.BasePresenter;
import com.dmilicic.weather.app.presentation.ui.BaseView;

import java.util.List;


public interface MainPresenter extends BasePresenter {

    interface View extends BaseView {
        void showBookmarks(List<Bookmark> bookmarks);

        void showWeatherData(Weather weather);

        void showMaps();

        void showHelp();
    }

    void getBookmarks();

    void saveLastScreen(int lastScreenIdx);

    void saveSelectedBookmark(Bookmark bookmark);

    void onBookmarkClicked(Bookmark bookmark);

    void retrieveWeatherData(String bookmarkName, double latitude, double longitude);

    void onLocationBookmarked(String bookmarkName, double latitude, double longitude);

    void onBookmarkDeleted(String bookmarkName);

}
