package com.dmilicic.weather.app.presentation.presenters.impl;

import com.dmilicic.weather.app.domain.model.Weather;
import com.dmilicic.weather.app.domain.repository.BookmarkRepository;
import com.dmilicic.weather.app.domain.repository.WeatherRepository;
import com.dmilicic.weather.app.domain.repository.WeatherRepository.Callback;
import com.dmilicic.weather.app.network.WeatherRepositoryImpl;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.presentation.presenters.MainPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dmilicic on 12/13/15.
 */
public class MainPresenterImpl implements MainPresenter {

    private MainPresenter.View mView;

    // last selected screen indices
    public static final int LIST_SCREEN_IDX = 1;
    public static final int MAPS_SCREEN_IDX = 2;
    public static final int WEATHER_SCREEN_IDX = 3;
    public static final int HELP_SCREEN_IDX = 3;

    private int mLastSelectedScreen;

    // this will serve as our in-memory cache
    private List<Bookmark>       mBookmarks;
    private Map<String, Weather> mWeatherMap;
    private Bookmark mSelectedBookmark;

    // this is our db
    private BookmarkRepository mBookmarkRepository;

    //region singleton
    private static MainPresenter sMainPresenter;

    public static MainPresenter getInstance(MainPresenter.View view, BookmarkRepository bookmarkRepository) {
        if (sMainPresenter == null) {
            sMainPresenter = new MainPresenterImpl();
        }

        // set required field members
        ((MainPresenterImpl)sMainPresenter).setView(view);
        ((MainPresenterImpl)sMainPresenter).setBookmarkRepository(bookmarkRepository);

        return sMainPresenter;
    }

    private MainPresenterImpl() {
        mBookmarks = new ArrayList<>();
        mWeatherMap = new HashMap<>();
    }

    public void setView(View view) {
        mView = view;
    }

    public void setBookmarkRepository(BookmarkRepository bookmarkRepository) {
        mBookmarkRepository = bookmarkRepository;
    }

    //endregion


    @Override
    public void resume() {

        // if the user has selected a bookmark before the activity died then restore that screen
        if (mLastSelectedScreen == WEATHER_SCREEN_IDX && mSelectedBookmark != null) {
            onBookmarkClicked(mSelectedBookmark);
            return;
        }

        // check if we were on the maps screen
        if (mLastSelectedScreen == MAPS_SCREEN_IDX) {
            mView.showMaps();
            return;
        }

        if (mLastSelectedScreen == HELP_SCREEN_IDX) {
            mView.showHelp();
            return;
        }

        // otherwise just show all bookmarks
        getBookmarks();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {
        mView = null;
    }

    @Override
    public void saveLastScreen(int lastScreenIdx) {
        mLastSelectedScreen = lastScreenIdx;
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void getBookmarks() {

        // check our cache first
        if (mView != null && mBookmarks != null && mBookmarks.size() > 0) {
            mView.showBookmarks(mBookmarks);
            return;
        }

        // if we have nothing cached then retrieve from the database
        if (mBookmarkRepository != null) {
            mBookmarkRepository.getAllBookmarks(new BookmarkRepository.Callback() {
                @Override
                public void onBookmarksRetrieved(List<Bookmark> bookmarks) {
                    mBookmarks = bookmarks; // cache them
                    if (mView != null) { // show them
                        mView.showBookmarks(mBookmarks);
                    }
                }
            });
        }
    }

    @Override
    public void saveSelectedBookmark(Bookmark bookmark) {
        mSelectedBookmark = bookmark;
    }

    @Override
    public void onBookmarkClicked(Bookmark bookmark) {
        mView.showProgress();

        // save user selection
        mSelectedBookmark = bookmark;

        retrieveWeatherData(bookmark.getBookmarkName(), bookmark.getLatitude(), bookmark.getLongitude(), new Callback() {
            @Override
            public void onWeatherRetrieved(Weather weather) {
                if (mView != null) {
                    mView.hideProgress();
                    mView.showWeatherData(weather);

                    mLastSelectedScreen = WEATHER_SCREEN_IDX;
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private void retrieveWeatherData(final String bookmarkName, double latitude, double longitude, Callback callback) {

        // get it from cache if possible so we don't do network calls
        Weather weather = mWeatherMap.get(bookmarkName);
        if (weather != null) {
            callback.onWeatherRetrieved(weather);
            return;
        }

        WeatherRepository weatherRepository = WeatherRepositoryImpl.getInstance();
        weatherRepository.getWeatherData(latitude, longitude, callback);
    }

    @Override
    public void retrieveWeatherData(final String bookmarkName, double latitude, double longitude) {
        retrieveWeatherData(bookmarkName, latitude, longitude, new Callback() {
            @Override
            public void onWeatherRetrieved(Weather weather) {
                mWeatherMap.put(bookmarkName, weather);

                // update bookmark data in the db
                if (mBookmarkRepository != null) {
                    // create a temporary object that will hold the data we need to update in our db
                    Bookmark updated = new Bookmark(bookmarkName, weather.getName(), weather.getLatitude(), weather.getLongitude());
                    mBookmarkRepository.updateBookmark(updated);
                }

                // update the actual name of the location
                if (mBookmarks != null) {
                    for (Bookmark bookmark : mBookmarks) {
                        if (bookmark.getBookmarkName().equals(bookmarkName)) {
                            bookmark.setLocationName(weather.getName());
                        }

                        // re-render the bookmarks list as we have new data to show
                        mView.showBookmarks(mBookmarks);
                    }
                }
            }

            @Override
            public void onError() {
            }
        });
    }

    @Override
    public void onLocationBookmarked(String bookmarkName, double latitude, double longitude) {

        Bookmark newBookmark = new Bookmark(bookmarkName, latitude, longitude);

        // add this to our cache of bookmarks
        mBookmarks.add(newBookmark);

        // retrieve weather info for this place
        retrieveWeatherData(bookmarkName, latitude, longitude);

        // show the new list
        // Note: this currently recreates the fragment to show one extra item, this is done for simplicity but can be optimized in case it gets laggy.
        // Optimization can be done by calling the appropriate recyclerview methods.
        mView.showBookmarks(mBookmarks);

        // remember that this is the last screen shown
        mLastSelectedScreen = LIST_SCREEN_IDX;

        // save the bookmark to db
        if (mBookmarkRepository != null) {
            mBookmarkRepository.saveBookmark(newBookmark);
        }
    }

    @Override
    public void onBookmarkDeleted(String bookmarkName) {

        // remove from our weather cache
        mWeatherMap.put(bookmarkName, null);

        // delete from db
        if (mBookmarkRepository != null) {
            mBookmarkRepository.deleteBookmarkByName(bookmarkName);
        }
    }
}
