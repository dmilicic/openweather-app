package com.dmilicic.weather.app.presentation.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.domain.model.Weather;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.presentation.presenters.MainPresenter;
import com.dmilicic.weather.app.presentation.presenters.MainPresenter.View;
import com.dmilicic.weather.app.presentation.presenters.impl.MainPresenterImpl;
import com.dmilicic.weather.app.presentation.ui.fragments.ListFragment;
import com.dmilicic.weather.app.presentation.ui.fragments.ListFragment.OnListFragmentInteractionListener;
import com.dmilicic.weather.app.presentation.ui.fragments.MapsFragment;
import com.dmilicic.weather.app.presentation.ui.fragments.MapsFragment.MapsFragmentIntegrationListener;
import com.dmilicic.weather.app.presentation.ui.fragments.PlaceFragment;
import com.dmilicic.weather.app.presentation.ui.fragments.WebviewFragment;
import com.dmilicic.weather.app.storage.BookmarkRepositoryImpl;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View,
        OnListFragmentInteractionListener, MapsFragmentIntegrationListener {

    private MainPresenter mMainPresenter;

    private int mCurrentFragmentIdx;
    private Fragment mCurrentFragment;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainPresenter = MainPresenterImpl.getInstance(this,
                new BookmarkRepositoryImpl(getApplicationContext()));

        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.help:
                showHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showHelp() {
        mCurrentFragmentIdx = MainPresenterImpl.HELP_SCREEN_IDX;
        mCurrentFragment = WebviewFragment.newInstance();
        navigateToFragment(mCurrentFragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainPresenter.resume();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save which fragment was opened
        mMainPresenter.saveLastScreen(mCurrentFragmentIdx);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMaps() {
        mCurrentFragmentIdx = MainPresenterImpl.MAPS_SCREEN_IDX;
        mCurrentFragment = MapsFragment.newInstance();
        navigateToFragment(mCurrentFragment);
    }

    @Override
    public void onLocationBookmarked(String bookmarkName, LatLng location) {
        mMainPresenter.onLocationBookmarked(bookmarkName, location.latitude, location.longitude);
    }

    @Override
    public void showBookmarks(List<Bookmark> bookmarks) {
        mCurrentFragmentIdx = MainPresenterImpl.LIST_SCREEN_IDX;
        mCurrentFragment = ListFragment.newInstance((ArrayList<Bookmark>) bookmarks);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mCurrentFragment)
                .commit();
    }

    @Override
    public void showWeatherData(Weather weather) {
        mCurrentFragmentIdx = MainPresenterImpl.WEATHER_SCREEN_IDX;
        mCurrentFragment = PlaceFragment.newInstance(weather);
        navigateToFragment(mCurrentFragment);
    }

    private void navigateToFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @Override
    public void onClickAddNewItem() {
        showMaps();
    }

    @Override
    public void onBookmarkClick(Bookmark bookmark) {
        mMainPresenter.onBookmarkClicked(bookmark);
    }

    @Override
    public void onDeleteBookmark(String name) {
        mMainPresenter.onBookmarkDeleted(name);

        // there is logic to do if the list becomes empty
        if (mCurrentFragment != null && mCurrentFragment instanceof ListFragment) {
            ((ListFragment)mCurrentFragment).checkIfEmptyList();
        }
    }

    @Override
    public void onBackPressed() {
        if (mCurrentFragmentIdx == MainPresenterImpl.LIST_SCREEN_IDX) {
            finish();
            return;
        }

        // clear user selection
        mMainPresenter.saveSelectedBookmark(null);
        mCurrentFragmentIdx = MainPresenterImpl.LIST_SCREEN_IDX;
        mMainPresenter.saveLastScreen(mCurrentFragmentIdx);

        // retrieve bookmarks for the bookmark screen
        mMainPresenter.getBookmarks();
    }
}
