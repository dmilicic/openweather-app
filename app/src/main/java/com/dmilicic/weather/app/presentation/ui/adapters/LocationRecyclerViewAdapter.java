package com.dmilicic.weather.app.presentation.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.presentation.ui.fragments.ListFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Bookmark} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class LocationRecyclerViewAdapter extends RecyclerView.Adapter<LocationRecyclerViewAdapter.ViewHolder> {

    private final List<Bookmark>                    mData;
    private final OnListFragmentInteractionListener mListener;

    public LocationRecyclerViewAdapter(List<Bookmark> items, OnListFragmentInteractionListener listener) {
        mData = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // in case we have reached the empty rows
        if (position >= mData.size()) {
            holder.mImageView.setVisibility(View.GONE); // make the button invisible on empty views
            return;
        }

        holder.mItem = mData.get(position);

        // if we have a location name then display in this format: bookmark_name (actual_location_name)
        String content = holder.mItem.getBookmarkName();
        String locationName = holder.mItem.getLocationName();
        if (locationName != null && locationName.length() > 0) {
            content = String.format("%s (%s)", content, locationName);
        }
        holder.mContentView.setText(content);


        holder.mImageView.setVisibility(View.VISIBLE); // avoid possible bugs

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onBookmarkClick(holder.mItem);
                }
            }
        });

        // setup delete button on click listener
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewPos = holder.getAdapterPosition();

                // notify our listeners
                if (mListener != null) {
                    Bookmark bookmark = mData.get(viewPos);
                    mData.remove(viewPos);
                    mListener.onDeleteBookmark(bookmark.getBookmarkName());
                }
                notifyItemRemoved(viewPos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size() + 1; // let's create an extra empty row so the FAB isn't on top of a list item
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View            mView;
        public final TextView        mContentView;
        public final ImageView       mImageView;
        public       Bookmark mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.content);
            mImageView = view.findViewById(R.id.delete_btn);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
