package com.dmilicic.weather.app.presentation.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.presentation.ui.listeners.BookmarkDialogListener;

/**
 * Created by dario on 15.09.2017..
 */

public class DialogFactory {

    public static void showBookmarkDialog(Context ctx, final BookmarkDialogListener listener) {
        final EditText edittext = new EditText(ctx);

        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setMessage(ctx.getString(R.string.enter_location));
        alert.setTitle(ctx.getString(R.string.new_bookmark));

        alert.setView(edittext);

        alert.setPositiveButton(ctx.getString(R.string.save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String bookmarkName = edittext.getText().toString();
                if (listener != null) { // notify our listener
                    listener.onBookmark(bookmarkName);
                }
            }
        });

        alert.setNegativeButton(ctx.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();
    }
}
