package com.dmilicic.weather.app.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.presentation.ui.adapters.LocationRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ListFragment extends Fragment {

    public static final String ARG_WEATHER_LIST = "weather_list_data_key";

    private FloatingActionButton              mFab;
    private RecyclerView                      mRecyclerView;
    private OnListFragmentInteractionListener mListener;

    private TextView mNoBookmarks;

    private ArrayList<Bookmark> mBookmarks;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListFragment() {
    }

    public static ListFragment newInstance(ArrayList<Bookmark> data) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_WEATHER_LIST, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mBookmarks = getArguments().getParcelableArrayList(ARG_WEATHER_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        mRecyclerView = view.findViewById(R.id.locations_recyclerview);
        mRecyclerView.setAdapter(new LocationRecyclerViewAdapter(mBookmarks, mListener));

        mNoBookmarks = view.findViewById(R.id.no_bookmarks_txtview);
        checkIfEmptyList();

        mFab = view.findViewById(R.id.fab);
        mFab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) { // notify the listener that we want to add a new weather location
                    mListener.onClickAddNewItem();
                }
            }
        });

        return view;
    }

    public void checkIfEmptyList() {
        // nothing to do if our textview is not initialized
        if (mNoBookmarks == null) return;

        // show the text depending on if there are any bookmarks
        if (mBookmarks == null || mBookmarks.size() == 0) {
            mNoBookmarks.setVisibility(View.VISIBLE);
        } else {
            mNoBookmarks.setVisibility(View.GONE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onDeleteBookmark(String name);

        void onBookmarkClick(Bookmark item);

        void onClickAddNewItem();
    }
}
