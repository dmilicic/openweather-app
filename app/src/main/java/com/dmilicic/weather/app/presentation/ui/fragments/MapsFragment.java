package com.dmilicic.weather.app.presentation.ui.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.presentation.ui.dialogs.DialogFactory;
import com.dmilicic.weather.app.presentation.ui.listeners.BookmarkDialogListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import static com.dmilicic.weather.app.R.id.map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapsFragmentIntegrationListener} interface
 * to handle interaction events.
 * Use the {@link MapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback, OnMapClickListener, BookmarkDialogListener {

    // A default location (Amsterdam) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(52.3702, 4.8952);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    private LatLng mSelectedLocation = mDefaultLocation;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    private GoogleMap                   mGmaps;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private FloatingActionButton mFab;

    private MapsFragmentIntegrationListener mListener;

    public MapsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapsFragment.
     */
    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maps, container, false);

        // Request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(map);
        mapFragment.getMapAsync(this);

        mFab = view.findViewById(R.id.fab);
        mFab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFactory.showBookmarkDialog(getContext(), MapsFragment.this);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MapsFragmentIntegrationListener) {
            mListener = (MapsFragmentIntegrationListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MapsFragmentIntegrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mGmaps != null) {
            mGmaps.clear();
            mGmaps = null;
        }
    }

    @Override
    public void onBookmark(String bookmarkName) {
        if (mListener != null && mSelectedLocation != null) {
            mListener.onLocationBookmarked(bookmarkName, mSelectedLocation);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGmaps = googleMap;
        googleMap.setOnMapClickListener(this);

        // TODO: set marker to device location
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        googleMap.addMarker(new MarkerOptions().position(mDefaultLocation)
                .title("Default location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(mDefaultLocation));

        googleMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if (mLastKnownLocation != null) {
                    placeAndMoveToMarker(new LatLng(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude()));
                    return true;
                }

                return false;
            }
        });

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        placeAndMoveToMarker(latLng);
    }

    private void placeAndMoveToMarker(LatLng latLng) {
        if (mGmaps != null) {

            // mark this location as selected
            mSelectedLocation = latLng;

            mGmaps.clear();
            mGmaps.addMarker(new MarkerOptions().position(latLng));
            mGmaps.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    private void getLocationPermission() {
    /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            default:
                break;
        }

        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mGmaps == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mGmaps.setMyLocationEnabled(true);
                mGmaps.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mGmaps.setMyLocationEnabled(false);
                mGmaps.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {
    /*
     * Get the best and most recent location of the device, which may be null in rare
     * cases when a location is not available.
     */
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        mLastKnownLocation = (Location) task.getResult();

                        if (mLastKnownLocation != null) {
                            // Set the map's camera position to the current location of the device.
                            placeAndMoveToMarker(new LatLng(mLastKnownLocation.getLatitude(),
                                    mLastKnownLocation.getLongitude()));
                        } else {
                            placeAndMoveToMarker(mDefaultLocation);
                            mGmaps.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface MapsFragmentIntegrationListener {
        void onLocationBookmarked(String bookmarkName, LatLng location);
    }
}
