package com.dmilicic.weather.app.presentation.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dmilicic.weather.app.R;
import com.dmilicic.weather.app.domain.model.Weather;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A fragment representing a weather object.
 */
public class PlaceFragment extends Fragment {

    public static final String ARG_WEATHER = "weather_data_key";

    private SimpleDateFormat mSdf = new SimpleDateFormat("MMMM dd ", Locale.ENGLISH);

    private Weather mWeather;

    private TextView mMainForecast;
    private TextView mDate;
    private TextView mTemperature;
    private TextView mHumidity;
    private TextView mWindSpeed;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlaceFragment() {
    }

    public static PlaceFragment newInstance(Weather weather) {
        PlaceFragment fragment = new PlaceFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_WEATHER, weather);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mWeather = getArguments().getParcelable(ARG_WEATHER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place, container, false);


        mMainForecast = view.findViewById(R.id.main_forecast);
        mMainForecast.setText(mWeather.getMainForecast());

        // display location name and date in the form of: loc_name, date
        mDate = view.findViewById(R.id.name_and_date);
        Date today = Calendar.getInstance().getTime();
        String nameAndDate = mSdf.format(today);
        if (mWeather.getName() != null && mWeather.getName().length() > 0) {
            nameAndDate = String.format("%s, %s", mWeather.getName(), nameAndDate);
        }
        mDate.setText(nameAndDate);

        mTemperature = view.findViewById(R.id.temperature_value);
        mTemperature.setText(String.format(Locale.UK, "%d", (int)mWeather.getTemperature()));

        mHumidity = view.findViewById(R.id.humidity);
        String humidity = getString(R.string.humidity, mWeather.getHumidity());
        mHumidity.setText(humidity);

        mWindSpeed = view.findViewById(R.id.wind_speed);
        String windspeed = getString(R.string.wind_speed, (int)mWeather.getWindSpeed());
        mWindSpeed.setText(windspeed);

        return view;
    }
}
