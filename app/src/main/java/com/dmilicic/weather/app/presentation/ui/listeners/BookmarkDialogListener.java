package com.dmilicic.weather.app.presentation.ui.listeners;

/**
 * Created by dario on 15.09.2017..
 */

public interface BookmarkDialogListener {
    void onBookmark(String bookmarkName);
}
