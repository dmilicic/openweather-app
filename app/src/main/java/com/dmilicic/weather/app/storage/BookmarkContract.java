package com.dmilicic.weather.app.storage;

import android.provider.BaseColumns;

import static com.dmilicic.weather.app.storage.BookmarkContract.BookmarkEntry.TABLE_NAME;

/**
 * Created by dario on 16.09.2017..
 */

public class BookmarkContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    BookmarkEntry._ID + " INTEGER PRIMARY KEY," +
                    BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME + " TEXT," +
                    BookmarkEntry.COLUMN_NAME_LOCATION_NAME + " TEXT," +
                    BookmarkEntry.COLUMN_NAME_LATITUDE + " DOUBLE," +
                    BookmarkEntry.COLUMN_NAME_LONGITUDE + " DOUBLE)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BookmarkEntry.TABLE_NAME;

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private BookmarkContract() {}

    /* Inner class that defines the table contents */
    public static class BookmarkEntry implements BaseColumns {
        public static final String TABLE_NAME                = "bookmarks";
        public static final String COLUMN_NAME_BOOKMARK_NAME = "name";
        public static final String COLUMN_NAME_LOCATION_NAME = "location_name";
        public static final String COLUMN_NAME_LATITUDE      = "latitude";
        public static final String COLUMN_NAME_LONGITUDE     = "longitude";
    }
}
