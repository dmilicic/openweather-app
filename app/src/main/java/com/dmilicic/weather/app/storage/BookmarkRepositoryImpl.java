package com.dmilicic.weather.app.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmilicic.weather.app.domain.repository.BookmarkRepository;
import com.dmilicic.weather.app.presentation.model.Bookmark;
import com.dmilicic.weather.app.storage.BookmarkContract.BookmarkEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dario on 16.09.2017..
 */

public class BookmarkRepositoryImpl implements BookmarkRepository {

    private DbHelper mDbHelper;

    public BookmarkRepositoryImpl(Context appContext) {
        mDbHelper = new DbHelper(appContext);
    }

    @Override
    public void getAllBookmarks(Callback callback) {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // we need bookmark name, lat and lon fields
        String[] projection = {
                BookmarkEntry._ID,
                BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME,
                BookmarkEntry.COLUMN_NAME_LOCATION_NAME,
                BookmarkEntry.COLUMN_NAME_LATITUDE,
                BookmarkEntry.COLUMN_NAME_LONGITUDE

        };

        // we will sort by bookmark name
        String sortOrder =
                BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME + " ASC";

        Cursor cursor = db.query(
                BookmarkEntry.TABLE_NAME,                 // The table to query
                projection,                               // The columns to return
                null,                                     // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        List<Bookmark> bookmarks = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(BookmarkEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME));
            String locationName = cursor.getString(cursor.getColumnIndexOrThrow(BookmarkEntry.COLUMN_NAME_LOCATION_NAME));
            double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(BookmarkEntry.COLUMN_NAME_LATITUDE));
            double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(BookmarkEntry.COLUMN_NAME_LONGITUDE));

            bookmarks.add(new Bookmark(name, locationName, lat, lon));
        }
        cursor.close();

        callback.onBookmarksRetrieved(bookmarks);
    }

    @Override
    public boolean saveBookmark(Bookmark bookmark) {

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME, bookmark.getBookmarkName());
        values.put(BookmarkEntry.COLUMN_NAME_LOCATION_NAME, bookmark.getLocationName());
        values.put(BookmarkEntry.COLUMN_NAME_LATITUDE, bookmark.getLatitude());
        values.put(BookmarkEntry.COLUMN_NAME_LONGITUDE, bookmark.getLongitude());

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(BookmarkEntry.TABLE_NAME, null, values);

        // something bad happened
        if (newRowId == -1)
            return false;

        return true;
    }

    @Override
    public boolean deleteBookmark(Bookmark bookmark) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Define 'where' part of query.
        String selection = BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { bookmark.getBookmarkName() };
        // Issue SQL statement.
        int rowsDeleted = db.delete(BookmarkEntry.TABLE_NAME, selection, selectionArgs);

        if (rowsDeleted > 0)
            return true;

        return false;
    }


    @Override
    public boolean deleteBookmarkByName(String bookmarkName) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Define 'where' part of query.
        String selection = BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { bookmarkName };
        // Issue SQL statement.
        int rowsDeleted = db.delete(BookmarkEntry.TABLE_NAME, selection, selectionArgs);

        if (rowsDeleted > 0)
            return true;

        return false;
    }

    @Override
    public boolean updateBookmark(Bookmark updated) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME, updated.getBookmarkName());
        values.put(BookmarkEntry.COLUMN_NAME_LOCATION_NAME, updated.getLocationName());
        values.put(BookmarkEntry.COLUMN_NAME_LATITUDE, updated.getLatitude());
        values.put(BookmarkEntry.COLUMN_NAME_LONGITUDE, updated.getLongitude());

        // which row to update, based on the bookmark name
        String selection = BookmarkEntry.COLUMN_NAME_BOOKMARK_NAME + " LIKE ?";
        String[] selectionArgs = { updated.getBookmarkName() };

        int count = db.update(
                BookmarkEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        if (count > 0)
            return true;

        return false;
    }
}
