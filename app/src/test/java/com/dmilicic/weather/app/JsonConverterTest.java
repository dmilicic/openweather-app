package com.dmilicic.weather.app;

import com.dmilicic.weather.app.domain.model.Weather;
import com.dmilicic.weather.app.network.converter.WeatherJsonConverter;

import junit.framework.Assert;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class JsonConverterTest {
    @Test
    public void conversion_is_successful() throws Exception {
        String exampleJson = "{  \n" +
                "   \"coord\":{  \n" +
                "      \"lon\":32.345,\n" +
                "      \"lat\":12.567\n" +
                "   },\n" +
                "   \"weather\":[  \n" +
                "      {  \n" +
                "         \"id\":800,\n" +
                "         \"main\":\"Clear\",\n" +
                "         \"description\":\"clear sky\",\n" +
                "         \"icon\":\"01d\"\n" +
                "      }\n" +
                "   ],\n" +
                "   \"base\":\"stations\",\n" +
                "   \"main\":{  \n" +
                "      \"temp\":24.17,\n" +
                "      \"pressure\":1028.42,\n" +
                "      \"humidity\":100,\n" +
                "      \"temp_min\":24.17,\n" +
                "      \"temp_max\":24.17,\n" +
                "      \"sea_level\":1028.56,\n" +
                "      \"grnd_level\":1028.42\n" +
                "   },\n" +
                "   \"wind\":{  \n" +
                "      \"speed\":2.41,\n" +
                "      \"deg\":237.501\n" +
                "   },\n" +
                "   \"clouds\":{  \n" +
                "      \"all\":0\n" +
                "   },\n" +
                "   \"dt\":1505392890,\n" +
                "   \"sys\":{  \n" +
                "      \"message\":0.0039,\n" +
                "      \"sunrise\":1505368324,\n" +
                "      \"sunset\":1505411914\n" +
                "   },\n" +
                "   \"id\":6295630,\n" +
                "   \"name\":\"Earth\",\n" +
                "   \"cod\":200\n" +
                "}";


        Weather weather = WeatherJsonConverter.fromJson(exampleJson);

        Assert.assertEquals("Earth", weather.getName());
        Assert.assertEquals(32.345, weather.getLongitude());
        Assert.assertEquals(12.567, weather.getLatitude());
        Assert.assertEquals("Clear", weather.getMainForecast());
        Assert.assertEquals("clear sky", weather.getDescription());
        Assert.assertEquals(24.17, weather.getTemperature());
        Assert.assertEquals(2.41, weather.getWindSpeed());

    }

    @Test
    public void conversion_is_successful2() throws Exception {
        String exampleJson = "{  \n" +
                "   \"coord\":{  \n" +
                "      \"lon\":15.9,\n" +
                "      \"lat\":45.8\n" +
                "   },\n" +
                "   \"weather\":[  \n" +
                "      {  \n" +
                "         \"id\":800,\n" +
                "         \"main\":\"Clear\",\n" +
                "         \"description\":\"clear sky\",\n" +
                "         \"icon\":\"01n\"\n" +
                "      }\n" +
                "   ],\n" +
                "   \"base\":\"stations\",\n" +
                "   \"main\":{  \n" +
                "      \"temp\":13,\n" +
                "      \"pressure\":1015,\n" +
                "      \"humidity\":82,\n" +
                "      \"temp_min\":13,\n" +
                "      \"temp_max\":13\n" +
                "   },\n" +
                "   \"visibility\":10000,\n" +
                "   \"wind\":{  \n" +
                "      \"speed\":1\n" +
                "   },\n" +
                "   \"clouds\":{  \n" +
                "      \"all\":0\n" +
                "   },\n" +
                "   \"dt\":1505500200,\n" +
                "   \"sys\":{  \n" +
                "      \"type\":1,\n" +
                "      \"id\":5461,\n" +
                "      \"message\":0.0038,\n" +
                "      \"country\":\"HR\",\n" +
                "      \"sunrise\":1505450139,\n" +
                "      \"sunset\":1505495166\n" +
                "   },\n" +
                "   \"id\":3198635,\n" +
                "   \"name\":\"Jezdovec\",\n" +
                "   \"cod\":200\n" +
                "}";


        Weather weather = WeatherJsonConverter.fromJson(exampleJson);

        Assert.assertEquals("Jezdovec", weather.getName());
        Assert.assertEquals(15.9, weather.getLongitude());
        Assert.assertEquals(45.8, weather.getLatitude());
        Assert.assertEquals("Clear", weather.getMainForecast());
        Assert.assertEquals("clear sky", weather.getDescription());
        Assert.assertEquals(13.0, weather.getTemperature());
        Assert.assertEquals(1.0, weather.getWindSpeed());
        Assert.assertEquals(-1.0, weather.getWindDeg());
    }


    @Test
    public void conversion_with_no_name() throws Exception {
        String exampleJson = "{  \n" +
                "   \"coord\":{  \n" +
                "      \"lon\":15.9,\n" +
                "      \"lat\":45.8\n" +
                "   },\n" +
                "   \"weather\":[  \n" +
                "      {  \n" +
                "         \"id\":800,\n" +
                "         \"main\":\"Clear\",\n" +
                "         \"description\":\"clear sky\",\n" +
                "         \"icon\":\"01n\"\n" +
                "      }\n" +
                "   ],\n" +
                "   \"base\":\"stations\",\n" +
                "   \"main\":{  \n" +
                "      \"temp\":13,\n" +
                "      \"pressure\":1015,\n" +
                "      \"humidity\":82,\n" +
                "      \"temp_min\":13,\n" +
                "      \"temp_max\":13\n" +
                "   },\n" +
                "   \"visibility\":10000,\n" +
                "   \"wind\":{  \n" +
                "      \"speed\":1\n" +
                "   },\n" +
                "   \"clouds\":{  \n" +
                "      \"all\":0\n" +
                "   },\n" +
                "   \"dt\":1505500200,\n" +
                "   \"sys\":{  \n" +
                "      \"type\":1,\n" +
                "      \"id\":5461,\n" +
                "      \"message\":0.0038,\n" +
                "      \"country\":\"HR\",\n" +
                "      \"sunrise\":1505450139,\n" +
                "      \"sunset\":1505495166\n" +
                "   },\n" +
                "   \"id\":3198635,\n" +
                "   \"cod\":200\n" +
                "}";


        Weather weather = WeatherJsonConverter.fromJson(exampleJson);

        Assert.assertEquals("Unknown", weather.getName());
        Assert.assertEquals(15.9, weather.getLongitude());
        Assert.assertEquals(45.8, weather.getLatitude());
        Assert.assertEquals("Clear", weather.getMainForecast());
        Assert.assertEquals("clear sky", weather.getDescription());
        Assert.assertEquals(13.0, weather.getTemperature());
        Assert.assertEquals(1.0, weather.getWindSpeed());
        Assert.assertEquals(-1.0, weather.getWindDeg());
    }
}